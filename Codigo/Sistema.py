from datetime import datetime
from datetime import timedelta

class Sistema:
	def __init__(self):
		self.__hora = now+timedelta(days=7)
		self.__participantes = []
	
	def addParticipante(self,participante):
		self.__participantes.append(participante)

	def getParticipantes(self):
		return self.__participantes

	def getHora(self):
		return self.__hora

	def setHora(self, hora):
		self.__hora = hora
