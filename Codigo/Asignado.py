class Asignado:
	def __init__(self):
		pass

	def cambiarFecha(self, sistema, fecha):
		try:
			sistema.setHora(fecha)
		except:
			print("Error, no se pudo cambiar la fecha")

	def reporteParticipantes(self, sistema):
		try:
			part = sistema.getParticipantes()
			for i in part:
				for j in range(len(i)):
					print(j," - ",end="")
				print()
		except:
			print("Error, no se pudo mostrar la lista de participantes.")