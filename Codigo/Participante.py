class Participante:
	def __init__(self):
		self.__curp
		self.__nombre
		self.__apaterno
		self.__amaterno
		self.__telefono
		self.__generacion
		self.__carrera
		self.__tipoInstitucion
		self.__institucionActivo

	def validarLongitud(self, nombre, longitud):
		if (len(nombre) < longitud):
			return True
		return False

	def validarLimites(self, valor, limit1, limit2):
		if (valor >= limit1 and valor <= limit2):
			return True
		return False

	def getCurp(self):
		return self.__curp
	def getNombre(self):
		return self.__nombre
	def getAPaterno(self):
		return self.__apaterno
	def getAMaterno(self):
		return self.__amaterno
	def getTelefono(self):
		return self.__telefono
	def getGeneracion(self):
		return self.__generacion
	def getCarrera(self):
		return self.__carrera
	def getTipoInstitucion(self):
		return self.__tipoInstitucion
	def getInstitucionActivo(self):
		return self.__institucionActivo

	def setCurp(self, curp):
		if(self.validarLimites(curp, 18, 18))
			self.__curp = curp
			return True
		return False

	def setNombre(self, nombre):
		if(self.validarLongitud(nombre, 51))
			self.__nombre = nombre
			return True
		return False
		
	def setAPaterno(self, apaterno):
		if(self.validarLongitud(apaterno, 51))
			self.__apaterno = apaterno
			return True
		return False

	def setAMaterno(self, amaterno):
		if(self.validarLongitud(amaterno, 51))
			self.__amaterno = amaterno
			return True
		return False

	def setTelefono(self, telefono):
		if(self.validarLimites(telefono, 10, 10))
			self.__telefono = telefono
			return True
		return False

	def setGeneracion(self, generacion):
		if(self.validarLimites(generacion, 1950, 2023))
			self.__generacion = generacion
			return True
		return False
	def setCarrera(self, carrera):
		self.__carrera = carrera
		return True

	def setTipoInstitucion(self, tipoInstitucion):
		if(self.validarLongitud(tipoInstitucion, 21))
			self.__tipoInstitucion = tipoInstitucion
			return True
		return False

	def setInstitucionActivo(self, institucionActivo):
		if (institucionActivo or not(institucionActivo)):
			self.__institucionActivo = institucionActivo
			return True
		return False
		